$( document ).ready(function() {

    var socket = io.connect('http://localhost:8890');

    socket.on('notification', function (data) {

        var message = JSON.parse(data);
        
        var html = '';
        
        console.log(message);
        var current_user_id = $('#current_user_id').val();
        console.log(current_user_id);
        
        if(message.sender_id == current_user_id){
            html = '<div class="message message-right "><div class="message-avatar"><div style="background-image:url(/img/chat-avatar_02.jpg);" class="b-avatar"></div></div>'
            + '<div class="message-body"><div data-dropdown-layout=".b-chat-messages" data-dropdown-group="message-user" class="message-header clearfix dropped">'
            + '<a href="#" class="message-name dropdown-link">' + message.sender_name + '</a>'
            + '<span class="message-time">' + message.time + '</span>'
            + '<ul class="message-menu dropdown-content chat-menu">'
            + '<li><a href="#" class="person"><i class="fa fa-newspaper-o"></i>Анкета</a></li>'
            + '<li><a href="#"><i class="fa fa-eye"></i>Лично</a></li>'
            + '<li><a href="#" class="ban"><i class="fa fa-ban"></i>Игнорировать</a></li>'
            + '<li><a href="#" class="warning"><i class="fa fa-exclamation-triangle"></i>Замечания(999)</a></li>'
            + '<li><a href="#" class="ban"><i class="fa fa-lock"></i>Бан</a></li>'
            + '<li><a href="#"><i class="fa fa-money"></i>Баланс:&ensp;100$</a></li>'
            + '</ul></div>'
            + '<div class="message-text">' + message.message + '</div>'
            + '</div></div>';
        }else{
            html = '<div class="message message-left ">'
            +'<div class="message-avatar"><div style="background-image:url(/img/chat-avatar_02.jpg);" class="b-avatar"></div></div>'
            +'<div class="message-body">'
            +'<div data-dropdown-layout=".b-chat-messages" data-dropdown-group="message-user" class="message-header clearfix dropped">'
            +'<a href="#" class="message-name dropdown-link">' + message.sender_name + '</a>'
            +'<span class="message-time">' + message.time + '</span>'
            +'<ul class="message-menu dropdown-content chat-menu">'
            +'<li><a href="#" class="person"><i class="fa fa-newspaper-o"></i>Анкета</a></li>'
            +'<li><a href="#"><i class="fa fa-eye"></i>Лично</a></li>'
            +'<li><a href="#" class="ban"><i class="fa fa-ban"></i>Игнорировать</a></li>'
            +'<li><a href="#" class="warning"><i class="fa fa-exclamation-triangle"></i>Замечания(999)</a></li>'
            +'<li><a href="#" class="ban"><i class="fa fa-lock"></i>Бан</a></li>'
            +'<li><a href="#"><i class="fa fa-money"></i>Баланс:&ensp;100$</a></li>'
            +'</ul></div>'
            +'<div class="message-text">' + message.message + '</div>'
            +'</div></div>';
        }
        
        $( "#notifications" ).append( html );

    });

});