<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

class SignupForm extends \dektrium\user\models\RegistrationForm
{
    public $password_conf;
    public $captcha;
    public $license_check;
    public $subscribe_news;

    public function rules()
    {
        $rules = parent::rules();

        $rules['usernameLength'] = ['username', 'string', 'min' => 4, 'max' => 255];
        $rules['passwordLength'] = ['password', 'string', 'min' => 8];
        
        $rules['password_confRequired'] = ['password_conf', 'required'];
        $rules['password_confCompare']  = [['password_conf'], 'compare', 'compareAttribute' => 'password', 'message'=>'asdasd'];
        
        $rules[] = ['captcha', 'required'];
        $rules[] = ['captcha', 'captcha', 'captchaAction' => 'site/captcha', 'caseSensitive' => false];
        $rules[] = ['license_check', 'required', 'message' => 'Вы должны принять условия соглашения'];
        $rules[] = ['license_check', 'boolean', 'message' => 'Вы должны принять условия соглашения'];
        $rules[] = [['license_check'], 'compare', 'compareValue' => true, 'message' => 'Вы должны принять условия соглашения'];
        return $rules;
    }
    
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['email'] = 'Email';
        $labels['username'] = 'Никнейм (Логин)';
        $labels['password'] = 'Пароль';        
        $labels['password_conf'] = 'Подтверждение пароля';        
        $labels['license_check'] = '';
        return $labels;
    }    
}