<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "member".
 *
 * @property integer $id
 * @property string $family
 * @property string $name
 * @property string $surname
 * @property integer $age
 * @property string $nationality
 * @property string $passport
 * @property string $passport_date
 * @property string $bithday
 * @property string $birthplace
 * @property string $country
 * @property string $city
 * @property string $education
 * @property string $education_last
 * @property string $profession
 * @property string $workplace
 * @property string $position
 * @property string $family_status
 * @property string $child
 * @property string $talants
 * @property string $habbits
 * @property string $advantages
 * @property string $disadvantages
 * @property string $crossgender
 * @property string $why
 * @property string $video
 * @property string $foto
 */
class Member extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['family', 'name', 'age', 'nationality', 'passport', 'passport_date'], 'required'],
            [['age'], 'integer'],
            [['family', 'name', 'surname', 'nationality', 'passport', 'passport_date', 'bithday', 'birthplace', 'country', 'city', 'education', 'education_last', 'profession', 'workplace', 'position', 'family_status', 'child', 'talants', 'habbits', 'advantages', 'disadvantages', 'crossgender', 'why', 'video', 'foto'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'family' => 'Family',
            'name' => 'Name',
            'surname' => 'Surname',
            'age' => 'Age',
            'nationality' => 'Nationality',
            'passport' => 'Passport',
            'passport_date' => 'Passport Date',
            'bithday' => 'Bithday',
            'birthplace' => 'Birthplace',
            'country' => 'Country',
            'city' => 'City',
            'education' => 'Education',
            'education_last' => 'Education Last',
            'profession' => 'Profession',
            'workplace' => 'Workplace',
            'position' => 'Position',
            'family_status' => 'Family Status',
            'child' => 'Child',
            'talants' => 'Talants',
            'habbits' => 'Habbits',
            'advantages' => 'Advantages',
            'disadvantages' => 'Disadvantages',
            'crossgender' => 'Crossgender',
            'why' => 'Why',
            'video' => 'Video',
            'foto' => 'Foto',
        ];
    }
}
