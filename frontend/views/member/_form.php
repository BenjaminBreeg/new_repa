<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Member */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="main-box main-custom newuser">
<div class="container">
    <div class="steps">
        <div class="step-col active">
            <div class="step-round">1</div>
            <div class="step-line"></div>
            <div class="step-text">Паспортные данные</div>
        </div><!--
         --><div class="step-col">
            <div class="step-round">2</div>
            <div class="step-line"></div>
            <div class="step-text">
            </div>
        </div><!--
         --><div class="step-col">
            <div class="step-round">3</div>
            <div class="step-line"></div>
            <div class="step-text">
            </div>
        </div>
    </div>
    <p  class="reg-title">Заполняются на каждого участника</p>

    <?php $form = ActiveForm::begin(['options' => ['class' => 'form']]); ?>

    <?= $form->field($model, 'family',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Фамилия", 'maxlength' => true]) ?>

    <?= $form->field($model, 'name',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Имя", 'maxlength' => true]) ?>

    <?= $form->field($model, 'surname',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Отчество", 'maxlength' => true]) ?>

    <?= $form->field($model, 'age',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput() ?>

    <?= $form->field($model, 'nationality',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Гражданство", 'maxlength' => true]) ?>

    <?= $form->field($model, 'passport',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Серия и номер паспорта", 'maxlength' => true]) ?>

    <?= $form->field($model, 'passport_date',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Дата выдачи (пример 06.07.1987)", 'maxlength' => true]) ?>

    <?= $form->field($model, 'bithday',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Дата рождения (пример 01.01.1901)", 'maxlength' => true]) ?>

    <?= $form->field($model, 'birthplace',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Место рождения", 'maxlength' => true]) ?>

    <?= $form->field($model, 'country',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Страна", 'maxlength' => true]) ?>

    <?= $form->field($model, 'city',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Город", 'maxlength' => true]) ?>

    <?= $form->field($model, 'education',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Образование",'maxlength' => true]) ?>

    <?= $form->field($model, 'education_last',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Название последнего учебного заведения",'maxlength' => true]) ?>

    <?= $form->field($model, 'profession',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Специальность",'maxlength' => true]) ?>

    <?= $form->field($model, 'workplace',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Место работы",'maxlength' => true]) ?>

    <?= $form->field($model, 'position',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Должность",'maxlength' => true]) ?>

    <?= $form->field($model, 'family_status',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Семейное положение",'maxlength' => true]) ?>

    <?= $form->field($model, 'child',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Есть ли у вас дети?",'maxlength' => true]) ?>

    <?= $form->field($model, 'talants',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Ваши таланты и увлечения",'maxlength' => true]) ?>

    <?= $form->field($model, 'habbits',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Ваши вредные привычки",'maxlength' => true]) ?>

    <?= $form->field($model, 'advantages',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Ваши достоинства",'maxlength' => true]) ?>

    <?= $form->field($model, 'disadvantages',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Ваши недостатки",'maxlength' => true]) ?>

    <?= $form->field($model, 'crossgender',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Что для вас важно в мужчине(женщине)?",'maxlength' => true]) ?>

    <?= $form->field($model, 'why',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Почему вы хотите принять участи в шоу \"соседи\"",'maxlength' => true]) ?>

    <?= $form->field($model, 'video',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "Видео о вас",'maxlength' => true]) ?>

    <?= $form->field($model, 'foto',
                ['template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>'])->textInput(['placeholder' => "фотошка",'maxlength' => true]) ?>

        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

    <?php ActiveForm::end(); ?>

</div>
