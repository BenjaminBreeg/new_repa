<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Members';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="member-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Member', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'family',
            'name',
            'surname',
            'age',
            // 'nationality',
            // 'passport',
            // 'passport_date',
            // 'bithday',
            // 'birthplace',
            // 'country',
            // 'city',
            // 'education',
            // 'education_last',
            // 'profession',
            // 'workplace',
            // 'position',
            // 'family_status',
            // 'child',
            // 'talants',
            // 'habbits',
            // 'advantages',
            // 'disadvantages',
            // 'crossgender',
            // 'why',
            // 'video',
            // 'foto',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
