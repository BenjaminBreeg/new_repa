<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Member */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Members', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="member-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'family',
            'name',
            'surname',
            'age',
            'nationality',
            'passport',
            'passport_date',
            'bithday',
            'birthplace',
            'country',
            'city',
            'education',
            'education_last',
            'profession',
            'workplace',
            'position',
            'family_status',
            'child',
            'talants',
            'habbits',
            'advantages',
            'disadvantages',
            'crossgender',
            'why',
            'video',
            'foto',
        ],
    ]) ?>

</div>
