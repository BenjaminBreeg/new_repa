<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Member */


 $this->title = 'Комната';
$js = <<<JS
$('#chat-form').submit(function() {

     var form = $(this);

     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: form.serialize(),
          success: function (response) {
               $("#message-field").val("");
          }
     });

     return false;
});
JS;
$this->registerJs($js, \yii\web\View::POS_READY) 
?>
<h1 class="header-title">
    <div class="vertical-center">ЗАХОДИ СЕГОДНЯ В ЧАТ....ТАМ ЕСТЬ Я... И Я ВАМ РАД :)</div>
</h1><!-- END header-title -->
<input type="hidden" id="current_user_id" name="current_user_id" value="<?=Yii::$app->user->getId()?>"/>
<div class="b-content clearfix">
    <div class="b-videos-col">
        <div id="js-videos" class="b-videos clearfix">
            <div class="b-videos-control clearfix">
                <span>ВИД:</span>
                <a href="#" data-view="1" class="view-link js-view view0 active"></a>
                <a href="#" data-view="2" class="view-link js-view view1"></a>
                <a href="#" data-view="4" class="view-link js-view view2"></a>
                <a href="#" data-view="1" data-refresh="1" class="view-link js-view refresh">
                    <i class="fa fa-refresh"></i>
                </a>
            </div>
            <div class="video-wrapper clearfix">
                <div class="video-wrapper-container">
                    <div class="video-box js-drop video-box-1">
                        <div data-ratio="0.5625" id="player-1" class="flowplayer">
                            <video>
                                <source type="application/x-mpegurl" src="http://wms.shared.streamshow.it/canale8/canale8/playlist.m3u8">
                            </video>
                        </div>
                        <div class="video-box-move"></div>
                    </div>
                    <div class="video-box js-drop video-box-2">
                        <div data-ratio="0.5625" id="player-2" class="flowplayer">
                            <video>
                                <source type="video/mp4" src="4na3.mp4"></video>
                            </div>
                            <div class="video-box-move"></div>
                        </div>
                        <div class="video-box js-drop video-box-3">
                            <div data-ratio="0.5625" id="player-3" class="flowplayer">
                                <video>
                                    <source type="application/x-mpegurl" src="http://wms.shared.streamshow.it/canale8/canale8/playlist.m3u8">
                                </video>
                            </div>
                            <div class="video-box-move"></div>
                        </div>
                        <div class="video-box js-drop video-box-4">
                            <div data-ratio="0.5625" id="player-4" class="flowplayer">
                                <video>
                                    <source type="application/x-mpegurl" src="http://wms.shared.streamshow.it/canale8/canale8/playlist.m3u8">
                                </video>
                            </div>
                            <div class="video-box-move"></div>
                        </div>
                    </div>
                </div>
                <div class="video-slider">
                    <a style="display:block;" class="slick-prev slick-arrow js-prev">
                        <span>Prev</span>
                    </a>
                    <a style="display:block;" class="slick-next slick-arrow js-next">
                        <span>Next</span>
                    </a>
                    <div class="video-slider-box jcarousel">
                        <ul>
                            <li class="slide">
                                <div style="background-image:url('/img/girl.jpg')" class="video-slider-slide">
                                    <span>Lorem Ipsum Dolor</span>
                                    <div class="video-box-move"></div>
                                </div>
                            </li>
                            <li class="slide">
                                <div style="background-image:url('/img/girl.jpg')" class="video-slider-slide">
                                    <span>Lorem Ipsum Dolor</span>
                                    <div class="video-box-move"></div>
                                </div>
                            </li><li class="slide"><div style="background-image:url('/img/girl.jpg')" class="video-slider-slide"><span>Lorem Ipsum Dolor</span><div class="video-box-move"></div></div></li><li class="slide"><div style="background-image:url('/img/girl.jpg')" class="video-slider-slide"><span>Lorem Ipsum Dolor</span><div class="video-box-move"></div></div></li><li class="slide"><div style="background-image:url('/img/girl.jpg')" class="video-slider-slide"><span>Lorem Ipsum Dolor</span><div class="video-box-move"></div></div></li><li class="slide"><div style="background-image:url('/img/girl.jpg')" class="video-slider-slide"><span>Lorem Ipsum Dolor</span><div class="video-box-move"></div></div></li><li class="slide"><div style="background-image:url('/img/girl.jpg')" class="video-slider-slide"><span>Lorem Ipsum Dolor</span><div class="video-box-move"></div></div></li><li class="slide"><div style="background-image:url('/img/girl.jpg')" class="video-slider-slide"><span>Lorem Ipsum Dolor</span><div class="video-box-move"></div></div></li></ul>
                    </div>
                    <div class="jcarousel-pagination slick-dots"></div>
                </div>
            </div>
        </div>
        <div id="js-chat" class="b-chat b-chat-col">
            <ul class="b-char-dots slick-dots">
                <li class="slick-active">
                    <button class="js-chat-open-messages"></button>
                </li>
                <li>
                    <button class="js-chat-open-users"></button>
                </li>
            </ul>
            <div id="js-chat-container" class="b-chat-container">
                <div class="b-chat-slider">
                    <div id="notifications" class="b-chat-messages b-chat-block">
                        <!--
                        <div class="message message-left ">
                            <div class="message-avatar">
                                <div style="background-image:url(/img/chat-avatar_02.jpg);" class="b-avatar"></div>
                            </div>
                            <div class="message-body">
                                <div data-dropdown-layout=".b-chat-messages" data-dropdown-group="message-user" class="message-header clearfix dropped">
                                    <a href="#" class="message-name dropdown-link">%Username%</a>
                                    <span class="message-time">02:13</span>
                                    <ul class="message-menu dropdown-content chat-menu">
                                        <li>
                                            <a href="#" class="person">
                                                <i class="fa fa-newspaper-o"></i>
                                                Анкета
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-eye"></i>
                                                Лично
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="ban">
                                                <i class="fa fa-ban"></i>
                                                Игнорировать
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="warning">
                                                <i class="fa fa-exclamation-triangle"></i>
                                                Замечания(999)
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="ban">
                                                <i class="fa fa-lock"></i>
                                                Бан
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-money"></i>
                                                Баланс:&ensp;100$
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="message-text">
                                    Lorem ipsum dolor sit &lt;i class=&quot;smiles smile1&quot;&gt;&lt;/i&gt; amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.
                                </div>
                            </div>
                        </div>
                        <div class="message message-left ">
                            <div class="message-avatar">
                                <div style="background-image:url(/img/chat-avatar_02.jpg);" class="b-avatar"></div>
                            </div>
                            <div class="message-body">
                                <div data-dropdown-layout=".b-chat-messages" data-dropdown-group="message-user" class="message-header clearfix dropped">
                                    <a href="#" class="message-name dropdown-link">%Username%</a>
                                    <span class="message-time">02:13</span>
                                    <ul class="message-menu dropdown-content chat-menu">
                                        <li>
                                            <a href="#" class="person">
                                                <i class="fa fa-newspaper-o"></i>
                                                Анкета
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-eye"></i>
                                                Лично
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="ban">
                                                <i class="fa fa-ban"></i>
                                                Игнорировать
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="warning">
                                                <i class="fa fa-exclamation-triangle"></i>
                                                Замечания(999)
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="ban">
                                                <i class="fa fa-lock"></i>
                                                Бан
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-money"></i>
                                                Баланс:&ensp;100$
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="message-text">
                                    Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.
                                </div>
                            </div>
                        </div>
                        <div class="message message-right ">
                            <div class="message-avatar">
                                <div style="background-image:url(/img/chat-avatar_02.jpg);" class="b-avatar">
                                </div>
                            </div>
                            <div class="message-body">
                                <div data-dropdown-layout=".b-chat-messages" data-dropdown-group="message-user" class="message-header clearfix dropped">
                                    <a href="#" class="message-name dropdown-link">%Username%</a>
                                    <span class="message-time">02:13</span>
                                    <ul class="message-menu dropdown-content chat-menu">
                                        <li>
                                            <a href="#" class="person">
                                                <i class="fa fa-newspaper-o"></i>
                                                Анкета
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-eye"></i>
                                                Лично
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="ban">
                                                <i class="fa fa-ban"></i>
                                                Игнорировать
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="warning">
                                                <i class="fa fa-exclamation-triangle"></i>
                                                Замечания(999)
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="ban">
                                                <i class="fa fa-lock"></i>
                                                Бан
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-money"></i>
                                                Баланс:&ensp;100$
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="message-text">
                                    Lorem ipsum dolor sit amet conse ctetur
                                </div>
                            </div>
                        </div>
                        <div class="message message-left warning">
                            <div class="message-avatar">
                                <div style="background-image:url(/img/chat-avatar_02.jpg);" class="b-avatar"></div>
                            </div>
                            <div class="message-body">
                                <div data-dropdown-layout=".b-chat-messages" data-dropdown-group="message-user" class="message-header clearfix dropped">
                                    <a href="#" class="message-name dropdown-link">%Username%</a>
                                    <span class="message-time">02:13</span>
                                    <ul class="message-menu dropdown-content chat-menu">
                                        <li>
                                            <a href="#" class="person">
                                                <i class="fa fa-newspaper-o"></i>
                                                Анкета
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-eye"></i>
                                                Лично
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="ban">
                                                <i class="fa fa-ban"></i>
                                                Игнорировать
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="warning">
                                                <i class="fa fa-exclamation-triangle"></i>
                                                Замечания(999)
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="ban">
                                                <i class="fa fa-lock"></i>
                                                Бан
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-money"></i>
                                                Баланс:&ensp;100$
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="message-text">Никнейм получил ЗАМЕЧАНИЕ</div>
                            </div>
                        </div>
                        <div class="message message-left ban">
                            <div class="message-avatar">
                                <div style="background-image:url(/img/chat-avatar_02.jpg);" class="b-avatar"></div>
                            </div>
                            <div class="message-body">
                                <div data-dropdown-layout=".b-chat-messages" data-dropdown-group="message-user" class="message-header clearfix dropped">
                                    <a href="#" class="message-name dropdown-link">%Username%</a>
                                    <span class="message-time">02:13</span>
                                    <ul class="message-menu dropdown-content chat-menu">
                                        <li>
                                            <a href="#" class="person">
                                                <i class="fa fa-newspaper-o"></i>Анкета
                                            </a>
                                        </li>
                                        <li><a href="#"><i class="fa fa-eye"></i>Лично</a></li>
                                        <li><a href="#" class="ban"><i class="fa fa-ban"></i>Игнорировать</a></li>
                                        <li><a href="#" class="warning"><i class="fa fa-exclamation-triangle"></i>Замечания(999)</a></li>
                                        <li><a href="#" class="ban"><i class="fa fa-lock"></i>Бан</a></li>
                                        <li><a href="#"><i class="fa fa-money"></i>Баланс:&ensp;100$</a></li>
                                    </ul>
                                </div>
                                <div class="message-text">Никнейм получил БАН</div>
                            </div>
                        </div>
                        <div class="message message-right person">
                            <div class="message-avatar">
                                <div style="background-image:url(/img/chat-avatar_02.jpg);" class="b-avatar"></div>
                            </div>
                            <div class="message-body">
                                <div data-dropdown-layout=".b-chat-messages" data-dropdown-group="message-user" class="message-header clearfix dropped">
                                    <a href="#" class="message-name dropdown-link">%Username%</a>
                                    <span class="message-time">02:13</span>
                                    <ul class="message-menu dropdown-content chat-menu"><li><a href="#" class="person"><i class="fa fa-newspaper-o"></i>Анкета</a></li><li><a href="#"><i class="fa fa-eye"></i>Лично</a></li><li><a href="#" class="ban"><i class="fa fa-ban"></i>Игнорировать</a></li><li><a href="#" class="warning"><i class="fa fa-exclamation-triangle"></i>Замечания(999)</a></li><li><a href="#" class="ban"><i class="fa fa-lock"></i>Бан</a></li><li><a href="#"><i class="fa fa-money"></i>Баланс:&ensp;100$</a></li></ul>
                                </div>
                                <div class="message-text">Lorem ipsum dolor sit amet conse ctetur</div>
                            </div>
                        </div>
                        -->
                    </div>
                    <div class="b-chat-users b-chat-block">
                        <div class="b-user">
                            <div class="b-user-avatar">
                                <div style="background-image:url(/img/chat-avatar_02.jpg);" class="b-avatar"></div>
                            </div>
                            <div class="b-user-body">
                                <div class="b-user-header clearfix">
                                    <div class="b-user-name admin">Администратор</div>
                                </div>
                                <div class="b-user-text">Как сделать многослойную Parallax иллюстрацию на CSS & JavaScript</div>
                            </div>
                            <ul class="b-user-menu chat-menu"><li><a href="#" class="person"><i class="fa fa-newspaper-o"></i><span>Анкета</span></a></li><li><a href="#"><i class="fa fa-eye"></i><span>Лично</span></a></li><li><a href="#" class="ban"><i class="fa fa-ban"></i><span>Игнорировать</span></a></li></ul>
                        </div>
                        <div class="b-user">
                            <div class="b-user-avatar">
                                <div style="background-image:url(/img/chat-avatar_02.jpg);" class="b-avatar"></div>
                            </div>
                            <div class="b-user-body">
                                <div class="b-user-header clearfix">
                                    <div class="b-user-name">User</div>
                                </div>
                                <div class="b-user-text">Как сделать многослойную Parallax иллюстрацию на CSS & JavaScript</div>
                            </div>
                            <ul class="b-user-menu chat-menu"><li><a href="#" class="person"><i class="fa fa-newspaper-o"></i><span>Анкета</span></a></li><li><a href="#"><i class="fa fa-eye"></i><span>Лично</span></a></li><li><a href="#" class="ban"><i class="fa fa-ban"></i><span>Игнорировать</span></a></li></ul>
                        </div>
                        <div class="b-user">
                            <div class="b-user-avatar">
                                <div style="background-image:url(/img/chat-avatar_02.jpg);" class="b-avatar"></div>
                            </div>
                            <div class="b-user-body">
                                <div class="b-user-header clearfix">
                                    <div class="b-user-name myname">My</div>
                                </div>
                                <div class="b-user-text">Как сделать многослойную Parallax иллюстрацию на CSS & JavaScript</div>
                            </div>
                            <ul class="b-user-menu chat-menu"><li><a href="#"><i class="fa fa-eye"></i><span>Лично</span></a></li><li><a href="#" class="ban"><i class="fa fa-lock"></i><span>Бан</span></a></li><li><a href="#" class="warning"><i class="fa fa-exclamation-triangle"></i><span>Замечания</span>(12)</a></li><li><a href="#"><i class="fa fa-money"></i><span>Баланс:&ensp;</span>$56</a></li></ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="b-chat-controls">
                <div class="chat-controls">
                    <?= Html::beginForm(['/member/room'], 'POST', [
                        'id' => 'chat-form',
                        'class'=>'dropped'
                    ]) ?>
                    <!--<form data-dropdown-layout=".chat-controls" class="dropped">-->
                        <?= Html::textArea('message', null, [
                            'id' => 'message-field',
                            'class' => '',
                            'placeholder' => ''
                        ]) ?>                        
                        <!--<textarea data-emojiable="true"></textarea>-->
                        <a href="#" class="chat-button resize-link js-chat-resize">
                            <i class="fa fa-expand"></i>
                            <i class="fa fa-compress"></i>
                        </a>
                        <a href="#" class="chat-button dropdown-link">
                            <i class="custom_font icon-smile"></i>
                        </a>
                        <a href="#" class="chat-button">
                            <i class="custom_font icon-eye"></i>
                        </a>
                        <span class="button-wrap">
                            <?= Html::input('submit', '', '') ?>                            
                            <!--<input type="submit" value="">-->
                            <i class="fa fa-envelope"></i>
                        </span>
                        <div class="dropdown-content smiles-box-wrapper">
                            <div class="smiles-box">
                                <div class="smiles smile0"></div>
                                <div class="smiles smile1"></div>
                                <div class="smiles smile2"></div>
                                <div class="smiles smile3"></div>
                                <div class="smiles smile4"></div>
                                <div class="smiles smile5"></div>
                                <div class="smiles smile6"></div>
                                <div class="smiles smile7"></div>
                                <div class="smiles smile8"></div>
                                <div class="smiles smile9"></div>
                                <div class="smiles smile10"></div>
                                <div class="smiles smile11"></div>
                                <div class="smiles smile12"></div>
                                <div class="smiles smile13"></div>
                                <div class="smiles smile14"></div>
                                <div class="smiles smile15"></div>
                                <div class="smiles smile16"></div>
                                <div class="smiles smile17"></div>
                                <div class="smiles smile18"></div>
                                <div class="smiles smile19"></div>
                                <div class="smiles smile20"></div>
                                <div class="smiles smile21"></div>
                                <div class="smiles smile22"></div>
                                <div class="smiles smile23"></div>
                                <div class="smiles smile24"></div>
                                <div class="smiles smile25"></div>
                                <div class="smiles smile26"></div>
                                <div class="smiles smile27"></div>
                                <div class="smiles smile28"></div>
                                <div class="smiles smile29"></div>
                                <div class="smiles smile30"></div>
                                <div class="smiles smile31"></div>
                                <div class="smiles smile32"></div>
                                <div class="smiles smile33"></div>
                                <div class="smiles smile34"></div>
                                <div class="smiles smile35"></div>
                                <div class="smiles smile36"></div>
                                <div class="smiles smile37"></div>
                                <div class="smiles smile38"></div>
                                <div class="smiles smile39"></div>
                                <div class="smiles smile40"></div>
                                <div class="smiles smile41"></div>
                                <div class="smiles smile42"></div>
                                <div class="smiles smile43"></div>
                                <div class="smiles smile44"></div>
                                <div class="smiles smile45"></div>
                                <div class="smiles smile46"></div>
                                <div class="smiles smile47"></div>
                                <div class="smiles smile48"></div>
                                <div class="smiles smile49"></div>
                                <div class="smiles smile50"></div>
                                <div class="smiles smile51"></div>
                                <div class="smiles smile52"></div>
                                <div class="smiles smile53"></div>
                                <div class="smiles smile54"></div>
                                <div class="smiles smile55"></div>
                                <div class="smiles smile56"></div>
                                <div class="smiles smile57"></div>
                                <div class="smiles smile58"></div>
                                <div class="smiles smile59"></div>
                                <div class="smiles smile60"></div>
                                <div class="smiles smile61"></div>
                                <div class="smiles smile62"></div>
                                <div class="smiles smile63"></div>
                                <div class="smiles smile64"></div>
                                <div class="smiles smile65"></div>
                                <div class="smiles smile66"></div>
                                <div class="smiles smile67"></div>
                                <div class="smiles smile68"></div>
                                <div class="smiles smile69"></div>
                                <div class="smiles smile70"></div>
                                <div class="smiles smile71"></div>
                                <div class="smiles smile72"></div>
                                <div class="smiles smile73"></div>
                                <div class="smiles smile74"></div>
                                <div class="smiles smile75"></div>
                                <div class="smiles smile76"></div>
                                <div class="smiles smile77"></div>
                            </div>
                        </div>
                    <?= Html::endForm() ?>
                </div>
            </div>
            <a class="slick-prev js-chat-slide">
                <span>Prev</span>
            </a>
            <a class="slick-next js-chat-slide">
                <span>Next</span>
            </a>
        </div>
    </div>
</div>
</div>
