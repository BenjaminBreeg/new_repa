<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Member */

$this->title = 'Личный кабинет';
?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>

