<?php
/**
 * Created by PhpStorm.
 * User: whisper
 * Date: 22.04.16
 * Time: 17:44
 */

/* @var $this yii\web\View */
/* @var $model app\models\Member */

$this->title = 'Личный кабинет';
?>
<div class="header-title">
    <div class="vertical-center">ЗАХОДИ СЕГОДНЯ В ЧАТ....ТАМ ЕСТЬ Я... И Я ВАМ РАД :)</div>
</div><!-- END header-title -->

<div class="main-box main-custom user-card ">
    <div class="container text-left">
        <div class="row"  style="margin-bottom: 15px;">
            <div class="col-md-12">

                <div class="media">
                    <h2 style="margin-bottom: 10px;">Pushkin</h2>
                    <div class="media-left " style="padding-right:0px;">
                        <img src="/img/man.png" alt="" style="margin-right: 5px;">
                    </div>
                    <div class="media-body">
                        <!--<p><span class="question">Никнейм:</span> Pushkin</p>-->
                        <p><span class="question">Фамилия:</span> Pushkin</p>
                        <p><span class="question">Имя:</span> Pushkin</p>
                        <p><span class="question">Отчество:</span> Pushkin</p>
                        <p><span class="question">Возраст:</span> Pushkin</p>
                        <div class="hidden-xs">
                            <p><span class="question">Ваша страна:</span> Pushkin</p>
                            <p><span class="question">Название последнего учебного заведения:</span> Pushkin</p>
                            <p><span class="question">Образование:</span> Pushkin</p>
                            <p><span class="question">Должность:</span> Pushkin</p>
                            <p><span class="question">Семейное положение:</span> Pushkin</p>
                            <p><span class="question">Есть ли у вас дети:</span> Pushkin</p>
                        </div>


                    </div>
                </div>

            </div>
            <div class="col-md-12 visible-xs">
                <div class="media-body">
                    <p><span class="question">Ваша страна:</span> Pushkin</p>
                    <p><span class="question">Название последнего учебного заведения:</span> Pushkin</p>
                    <p><span class="question">Образование:</span> Pushkin</p>
                    <p><span class="question">Должность:</span> Pushkin</p>
                    <p><span class="question">Семейное положение:</span> Pushkin</p>
                    <p><span class="question">Есть ли у вас дети:</span> Pushkin</p>
                </div>
            </div>
        </div>


        <div class="question-box">
            <p class="question">Мои таланты и увлечения:</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni aut, obcaecati voluptate eius accusamus enim nesciunt. Nesciunt veritatis dolor commodi, iusto soluta! Quo earum repudiandae labore tempora laboriosam excepturi voluptate.</p>
        </div>
        <div class="question-box">
            <p class="question">Мои вредные привычки:</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni aut, obcaecati voluptate eius accusamus enim nesciunt. Nesciunt veritatis dolor commodi, iusto soluta! Quo earum repudiandae labore tempora laboriosam excepturi voluptate.</p>
        </div>
        <div class="question-box">
            <p class="question">Мои достоинства:</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni aut, obcaecati voluptate eius accusamus enim nesciunt. Nesciunt veritatis dolor commodi, iusto soluta! Quo earum repudiandae labore tempora laboriosam excepturi voluptate.</p>
        </div>
        <div class="question-box">
            <p class="question">Мои недостатки:</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni aut, obcaecati voluptate eius accusamus enim nesciunt. Nesciunt veritatis dolor commodi, iusto soluta! Quo earum repudiandae labore tempora laboriosam excepturi voluptate.</p>
        </div>

        <p><span class="question">Что для Вас важно в мужчине (женщине)?:</span> Все</p>
        <p><span class="question">Почему Вы принимаете участие в шоу СОСЕДИ?:</span>  Потому что!</p>
        <div class="empty"></div>

        <div><span class="question">Краткое видео обо мне:</span> <div class="empty hidden-lg hidden-md hidden-sm"></div> <a href="#" class="page-button">Посмотреть</a> <a href='#delete' data-toggle="modal"  class="page-button red-btn">Удалить</a></div>

    </div>
</div><!-- END main-box -->


