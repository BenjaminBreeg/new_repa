<?php
use yii\helpers\Html;
?>
<div class="news-col">
    <div class="news-col-header"><?=$model->title?> <span class="page-link text-normal"><?=$model->created_at?></span></div>
    <p><?=$model->text?></p>
</div><!-- END news-col -->