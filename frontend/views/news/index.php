<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\LinkPager;
/* @var $this yii\web\View */
/* @var $model app\models\Member */

$this->title = "Новости";
?>
<div class="main-box main-custom">
    <div class="container">
        <div class="tab-content">
            <div class="tab-pane fade in active" id="info-news">
                <?= ListView::widget([
                    'dataProvider' 	=> $dataProvider,
                    'itemView' 		=> '_item',
                    'layout'		=> '{items}',
                    'itemOptions' 	=> ['class'=>'news-col'],
                ]);?>
                <?php echo LinkPager::widget([
                    'pagination' => $dataProvider->pagination
                ]);?>
            </div>
            <div class="tab-pane fade" id="info-question">.2..</div>
            <div class="tab-pane fade" id="info-rules">.3..</div>
            <div class="tab-pane fade" id="info-instructions">.4..</div>
        </div> 
    </div>
</div>
<!-- END main-box -->