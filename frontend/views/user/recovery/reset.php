<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/*
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\RecoveryForm $model
 */

$this->title = 'Сброс пароля';
?>
<div class="header-title">
    <div class="vertical-center">
        СБРОСИТЬ ПАРОЛЬ
    </div>
</div>

<div class="main-box main-custom" style="min-height: calc(100% - 237px);">
    <div class="container text-center">
        <?php $form = ActiveForm::begin([
            'id'                     => 'password-recovery-form',
            'enableAjaxValidation'   => true,
            'enableClientValidation' => false,
            'options' => ['class' => 'form'],
        ]); ?>

        <?= $form
            ->field($model, 'password', ['template' => '{error}{input}<i class="fa fa-lock"></i>'])
            ->passwordInput(['placeholder' => "Пароль"])
            ->error(['tag'=>'div', 'class'=>'help-block help-block-error'])?>


        <?= Html::submitButton(Yii::t('user', 'Finish'), ['class' => 'btn btn-success']) ?><br>

        <?php ActiveForm::end(); ?>
    </div>
</div>

