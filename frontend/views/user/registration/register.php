<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View              $this
 * @var dektrium\user\models\User $user
 * @var dektrium\user\Module      $module
 */

$this->title = 'Регистрация';
$this->registerJs('
jQuery(document.body).on("click", "#captcha-refresh", function(e) {
    jQuery("#register-form-captcha-image").yiiCaptcha("refresh");
    return false;
});
jQuery(document.body).on("click.yiiCaptcha", "#register-form-captcha-image", function(e) {
    e.preventDefault();
    console.log("asd");
    return false;
});
');
?>

<div class="header-title">
    <div class="vertical-center">РЕГИСТРАЦИЯ</div>
</div>
<div class="main-box main-custom">
    <div class="container text-center">
        <?php $form = ActiveForm::begin([
            'id'                     => 'form-signup',
            'enableAjaxValidation'   => true,
            'enableClientValidation' => false,
            //'validationUrl' => ['/site/signupvalidate'],
            'options' => ['class' => 'form']
        ]); ?>

        <?= $form
            ->field($model, 'username', ['template' => '{error}{input}<i class="fa fa-user"></i>'])
            ->textInput(['placeholder' => "Никнейм (Логин)"])
            ->error(['tag'=>'div', 'class'=>'help-block help-block-error'])?>
            
        <?php if ($module->enableGeneratingPassword == false): ?>
        <?= $form
            ->field($model, 'password', ['template' => '{error}{input}<i class="fa fa-lock"></i>'])
            ->passwordInput(['placeholder' => "Пароль"])
            ->error(['tag'=>'div', 'class'=>'help-block help-block-error'])?>

        <?= $form
            ->field($model, 'password_conf', ['template' => '{error}{input}<i class="fa fa-lock"></i>'])
            ->passwordInput(['placeholder' => "Подтверждение пароля"])
            ->error(['tag'=>'div', 'class'=>'help-block help-block-error'])?>
        <?php endif ?>

        <?= $form
            ->field($model, 'email', ['template' => '{error}{input}<i class="fa fa-envelope"></i>'])
            ->input('email', ['placeholder' => "E-mail"])
            ->error(['tag'=>'div', 'class'=>'help-block help-block-error'])?>

        <?= \yii\captcha\Captcha::widget([
                'captchaAction' => ['/site/captcha'],
                'model' => $model,
                'attribute'=>'captcha',
                'imageOptions' => ['class' => 'captcha-img', 'width' => '136px', 'height' => '43px'],
                'options' => ['showRefreshButton' => false],
                'value' => 'form-control vissible-390',
                'template' => '{image}<a href="#" id="captcha-refresh" class="captcha-refresh"><i class="fa fa-refresh captcha-refresh"></i></a>'
        ])?>

        <?= $form
            ->field($model, 'captcha', ['options' => ['class'=>'form-group captcha-code'], 'template' => '{error}{input}'])
            ->textInput(['class'=>'form-control hidden-390','placeholder' => "Код с картинки"])
            ->error(['tag'=>'div', 'class'=>'help-block help-block-error'])?>            
        
        <ul class="text-left check-wrapper">
            <li>
                <?= $form
                    ->field($model, 'license_check', ['options'=> ['wrapper'=>false], 'template'=>'{error}{input}{label}'])
                    ->checkbox([], false)
                    ->label('Я принимаю условия <a href="#" class="page-link">Соглашения</a>')
                ?>
            </li>
            <li class="empty"></li>
            <li>
                <?= $form
                    ->field($model, 'subscribe_news', ['options'=> ['wrapper'=>false], 'template'=>'{input}{label}'])
                    ->checkbox([], false)
                    ->label('Новости? Мне интересно!')
                ?>
            </li>
            <li class="empty"></li>
        </ul>

        <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>
