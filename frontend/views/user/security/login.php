<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\widgets\Connect;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View                   $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module           $module
 */

$this->title = 'Вход';
?>

<div class="header-title">
    <div class="vertical-center">
        ВХОД
    </div>
</div>

    <div class="main-box main-custom" style="min-height: calc(100% - 237px);">
        <div class="container text-center">
                <?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>
                <?php $form = ActiveForm::begin([
                    'id'                     => 'login-form',
                    'enableAjaxValidation'   => true,
                    'enableClientValidation' => false,
                    'validateOnBlur'         => false,
                    'validateOnType'         => false,
                    'validateOnChange'       => false,
                    'options' => ['class' => 'form'],
                ]) ?>

                    <?= $form
                        ->field($model, 'login', ['template' => '{error}{input}<i class="fa fa-user"></i>'])
                        ->textInput(['placeholder' => "Никнейм"])
                        ->error(['tag'=>'div', 'class'=>'help-block help-block-error'])?>
                        
                    <?= $form
                        ->field($model, 'password', ['template' => '{error}{input}<i class="fa fa-lock"></i>'])
                        ->passwordInput(['placeholder' => "Пароль"])
                        ->error(['tag'=>'div', 'class'=>'help-block help-block-error'])?>

                    <ul class="text-left check-wrapper">
                        <li>
                            <?= Html::a('Напомнить пароль', ['/forgot'], ['class' => 'page-link pull-right']) ?>
                            
                            <?= $form
                                ->field($model, 'rememberMe', ['options'=> ['wrapper'=>false], 'template'=>'{input}{label}'])
                                ->checkbox([], false)
                            ?>
                        </li>
                        <li class="empty"></li>
                    </ul>
                    <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <?php if ($module->enableConfirmation): ?>
            <p class="text-center">
                <?= Html::a(Yii::t('user', 'Didn\'t receive confirmation message?'), ['/resend']) ?>
            </p>
        <?php endif ?>
        <?php if ($module->enableRegistration): ?>
            <p class="text-center">
                <?= Html::a(Yii::t('user', 'Don\'t have an account? Sign up!'), ['/registration']) ?>
            </p>
        <?php endif ?>
        <?= Connect::widget([
            'baseAuthUrl' => ['/user/security/auth'],
        ]) ?>
    </div>
</div>
