<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div class="header-title">
    <div class="vertical-center">
        Смена пароля
    </div>
</div>
<div class="site-reset-password">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Новый пароль:</p>

    <div class="main-box main-custom" style="min-height: calc(100% - 237px);">
        <div class="container text-center">
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form', 'options' => ['class' => 'form'], 'fieldConfig' => ['template' => '{input}']]); ?>

                <?= $form->field($model, 'password', ['template' => '<div class="form-group">{input}<i class="fa fa-lock"></i></div>'])->passwordInput(['autofocus' => true]) ?>
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
