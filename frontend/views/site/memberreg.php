<?php
/**
 * Created by PhpStorm.
 * User: whisper
 * Date: 22.04.16
 * Time: 13:02
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Member */
/* @var $form ActiveForm */
?>
<div class="member-register">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'family') ?>
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'age') ?>
    <?= $form->field($model, 'nationality') ?>
    <?= $form->field($model, 'passport') ?>
    <?= $form->field($model, 'passport_date') ?>
    <?= $form->field($model, 'surname') ?>
    <?= $form->field($model, 'bithday') ?>
    <?= $form->field($model, 'birthplace') ?>
    <?= $form->field($model, 'country') ?>
    <?= $form->field($model, 'city') ?>
    <?= $form->field($model, 'education') ?>
    <?= $form->field($model, 'education_last') ?>
    <?= $form->field($model, 'profession') ?>
    <?= $form->field($model, 'workplace') ?>
    <?= $form->field($model, 'position') ?>
    <?= $form->field($model, 'family_status') ?>
    <?= $form->field($model, 'child') ?>
    <?= $form->field($model, 'talants') ?>
    <?= $form->field($model, 'habbits') ?>
    <?= $form->field($model, 'advantages') ?>
    <?= $form->field($model, 'disadvantages') ?>
    <?= $form->field($model, 'crossgender') ?>
    <?= $form->field($model, 'why') ?>
    <?= $form->field($model, 'video') ?>
    <?= $form->field($model, 'foto') ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- member-register -->