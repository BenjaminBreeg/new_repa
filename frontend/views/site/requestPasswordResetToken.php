<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div class="header-title">
    <div class="vertical-center">
        Восстановление пароля
    </div>
</div>
<div class="site-request-password-reset">
    <h1><?= Html::encode($this->title) ?></h1>



    <div class="main-box main-custom" style="min-height: calc(100% - 237px);">
        <div class="container text-center">
            <p>Пожалуйста, введите свой e-mail для получения ссылки восстановления.</p>
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form', 'options' => ['class' => 'form'], 'fieldConfig' => ['template' => '{input}']]); ?>

                <?= $form
                    ->field($model, 'email', ['template' => '{error}{input}<i class="fa fa-envelope"></i>'])
                    ->input('email', ['autofocus' => true,'placeholder' => "E-mail"])
                    ->error(['tag'=>'div'])?>


                <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
