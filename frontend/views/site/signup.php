<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = "Регистрация";

$this->registerJs('
jQuery(document.body).on("click", "#captcha-refresh", function(e) {
    jQuery("#signupform-captcha-image").yiiCaptcha("refresh");
    return false;
});
jQuery(document.body).on("click.yiiCaptcha", "#signupform-captcha-image", function(e) {
    e.preventDefault();
    console.log("asd");
    return false;
});
');

?>
<div class="header-title">
    <div class="vertical-center">РЕГИСТРАЦИЯ</div>
</div>
<div class="main-box main-custom">
    <div class="container text-center">
        <?php $form = ActiveForm::begin(['id' => 'form-signup', 'validationUrl'=> Url::to(['/site/signupvalidate']), 'enableAjaxValidation' => true, 'options' => ['class' => 'form']]); ?>
            <?= $form
                ->field($model, 'username', ['template' => '{error}{input}<i class="fa fa-user"></i>'])
                ->textInput(['placeholder' => "Никнейм (Логин)"])
                ->error(['tag'=>'div'])?>
                
            <?= $form
                ->field($model, 'password', ['template' => '{error}{input}<i class="fa fa-lock"></i>'])
                ->passwordInput(['placeholder' => "Пароль"])
                ->error(['tag'=>'div'])?>

            <?= $form
                ->field($model, 'password_conf', ['template' => '{error}{input}<i class="fa fa-lock"></i>'])
                ->passwordInput(['placeholder' => "Подтверждение пароля"])
                ->error(['tag'=>'div'])?>

            <?= $form
                ->field($model, 'email', ['template' => '{error}{input}<i class="fa fa-envelope"></i>'])
                ->input('email', ['placeholder' => "E-mail"])
                ->error(['tag'=>'div'])?>

            <?= \yii\captcha\Captcha::widget([
                    'model' => $model,
                    'attribute'=>'captcha',
                    'imageOptions' => ['class' => 'captcha-img', 'width' => '136px', 'height' => '43px'],
                    'options' => ['showRefreshButton' => false],
                    'value' => 'form-control vissible-390',
                    'template' => '{image}<a href="#" id="captcha-refresh" class="captcha-refresh"><i class="fa fa-refresh captcha-refresh"></i></a>'
            ])?>

            <?= $form
                ->field($model, 'captcha', ['options' => ['class'=>'form-group captcha-code'], 'template' => '{error}{input}'])
                ->textInput(['class'=>'form-control hidden-390','placeholder' => "Код с картинки"])
                ->error(['tag'=>'div'])?>
            
            <ul class="text-left check-wrapper">
                <li>
                    <?= $form
                        ->field($model, 'license_check', ['options'=> ['wrapper'=>false]])
                        ->checkbox(['template'=>'{error}{input}{beginLabel}{labelTitle}{endLabel}'])
                        ->label('Я принимаю условия <a href="#" class="page-link">Соглашения</a>')
                    ?>
                </li>
                <li class="empty"></li>
                <li>
                    <?= $form
                        ->field($model, 'subscribe_news', ['options'=> ['wrapper'=>false]])
                        ->checkbox(['template'=>'{input}{beginLabel}{labelTitle}{endLabel}'])
                        ->label('Новости? Мне интересно!')
                    ?>
                </li>
                <li class="empty"></li>
            </ul>
            <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>


        <?php ActiveForm::end(); ?>
    </div>
</div>
