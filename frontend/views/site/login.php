<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Войти';
?>
<div class="header-title">
    <div class="vertical-center">
        ВХОД
    </div>
</div>

    <div class="main-box main-custom" style="min-height: calc(100% - 237px);">
        <div class="container text-center">
            <?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['class' => 'form'], 'fieldConfig' => ['template' => '{input}']]); ?>

                <?= $form
                    ->field($model, 'username', ['template' => '{error}{input}<i class="fa fa-user"></i>'])
                    ->textInput(['placeholder' => "Никнейм"])
                    ->error(['tag'=>'div'])?>
                    
                <?= $form
                    ->field($model, 'password', ['template' => '{error}{input}<i class="fa fa-lock"></i>'])
                    ->passwordInput(['placeholder' => "Пароль"])
                    ->error(['tag'=>'div'])?>

                <ul class="text-left check-wrapper">
                    <li>
                        <?= Html::a('Напомнить пароль', ['site/request-password-reset'], ['class' => 'page-link pull-right']) ?>
                        
                        <?= $form
                            ->field($model, 'rememberMe', ['options'=> ['wrapper'=>false]])
                            ->checkbox(['template'=>'{input}{beginLabel}{labelTitle}{endLabel}'])
                        ?>
                    </li>
                    <li class="empty"></li>
                </ul>
                <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

