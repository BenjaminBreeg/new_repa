<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Пополнить баланс';

?>
    <div class="main-box main-custom balance ">
        <div class="container text-left" style="padding: 0 10px">
            <div class="balance-title">
                Webmoney <img src="/img/balance/webmoney.png" height="32" width="48" alt="">
            </div>
            
            <p> Создав электронный кошелек Webmoney, Вы сможете не только удобно, но и выгодно пополнять свой счет, комиссия Webmoney составляет всего 0,8%. <a href="">Зарегестирироваться.</a></p>

            <p>Если у Вас уже есть кошелек Webmoney, введите необходимую сумму,  выберите валюту для пополнения счета и нажмите <b>“Пополнить счет”</b></p>
            <form action="">
                <div class="form-group">
                    <input type="text" placeholder="Сумма">
                    <i class="fa fa-money"></i>
                </div>
                <div class="form-group">
                    <select class="selectpicker" title="Валюта">
                        <option data-content="<span class='option'>WMR</span>"></option>
                        <option data-content="<span class='option'>WMZ</span>"></option>
                    </select>
                </div>
                <span class="balance-item">=</span>
                <div class="form-group">
                    <input type="text" placeholder="100 монет" class="money">
                </div>
                <button type="submit" class="page-button">Пополнить счет</button>
            </form>

            <div class="balance-title">Банковская карта <img src="/img/balance/master-card.png" alt=""> <img src="/img/balance/visa.png" alt=""></div>
            
            <p>После нажатия кнопки “Пополнить счет” Вы будете перенаправлены на страницу, защищенную SSL сертификатом, который обеспечивает надежную защииту всех передаваемых данных. Все введенные на этой странице данные строго конфиденциальны, и ни при каких обстоятельствах не могут стать доступны третьим лицам. При первом пополнении банковской картой Вам понадобится ввести данные по картре (номер, имя пользователя и т.д.) Для последующих пополнений достаточно будет лишь указать последние 6 цифр номера карты.</p>
            
            <p>Выберите сумму поплнения и нажмите <b>“Пополнить счет”</b></p>

            <form action="">
                <div class="form-group">
                    <input type="text" placeholder="Сумма">
                    <i class="fa fa-money"></i>
                </div>
                <span class="balance-item">=</span>
                <div class="form-group">
                    <input type="text" placeholder="100 монет" class="money">
                </div>
                <button type="submit" class="page-button">Пополнить счет</button>
                <p>Для обеспечения безопасности при первом пополнении счета с карты, монеты на Ваш счет будут начислены с задержкой 10-15 минут, это время необходимо нам для проверки карты. Все последующие пополнения Вы сможете проводить без ограничений. Эта мера введена в целях защиты от мошенничества, просим отнестись с пониманием.</p>
            </form>

            <div class="balance-title">Яндекс.Деньги, QIWI и другие способы <img src="/img/balance/yad.png" alt=""> <img src="/img/balance/qiwi.png" alt=""></div>

            <p>Пополнение через платежную систему Oplata.info<br>
            После нажатия кнопки “Пополнить счет” Вы будете перенаправлены на страницу платежной системы Oplata.info. Этот платежный сервис предлагает множество альтернативных вариантов оплаты. После завершения операции оплаты Вы будете перенаправлены на страницу своего аккаунта на нашем сайте</p>

            <p>Выберите вариаеи оплаты, сумму поплнения и нажмите “Пополнить счет”</p>

            <form action="">
                <div class="form-group ya-form" style="width: 284px">
                    <select class="selectpicker" title="Яндекс деньги">
                        <option data-content="<span class='option'>Яндекс деньги</span>"></option>
                        <option data-content="<span class='option'>Яндекс деньги</span>"></option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" placeholder="Сумма">
                    <i class="fa fa-money"></i>
                </div>

                <span class="balance-item">=</span>
                <div class="form-group">
                    <input type="text" placeholder="" class="money">
                </div>
                <button type="submit" class="page-button">Пополнить счет</button>
            </form>

            <div class="balance-title">Мобильный телефон <img src="/img/balance/mobile.png" alt=""></div>
            <p>Оплата со счета Вашего мобильного телефона</p>
            <p>Выберите страну и оператора</p>

            <form action="" class="noborder">
                <div class="form-group mobile">
                    <select class="selectpicker" title="Ru - Россия">
                        <option data-content="<span class='option'>Ru - Россия</span>"></option>
                        <option data-content="<span class='option'>En - English</span>"></option>
                    </select>
                </div>
                <div class="form-group mobile">
                    <select class="selectpicker" title="Билайн">
                        <option data-content="<span class='option'>Билайн</span>"></option>
                        <option data-content="<span class='option'>Мегафон</span>"></option>
                    </select>
                </div>
                <p>Если  Вы выбраль этот способ оплаты, убедитесь, что на счете Вашего телефона есть необходимая сумма. После нажатия кнопки “Пополнить счет” Вы получите SMS с запросом подтверждения оплаты. Для подтверждения Вам понадобится отправить ответное SMS с любым текстом. После этого вас счет моментально будет пополнен.</p>

                <p>Ввелите номер своего мобильного телефона в формате 79ххххххххх</p>

                <div class="form-group phone" style="width: 284px">
                    <input type="text" placeholder="Мобильный телефон">
                    <i class="fa fa-mobile"></i>
                </div>

                <p>Введите сумму пополнения и нажмите <b>“Пополнить счет”</b></p>

                <div class="form-group">
                    <input type="text" placeholder="Сумма" class="">
                    <i class="fa fa-money"></i>
                </div>
                <span class="balance-item">=</span>
                <div class="form-group">
                    <input type="text" class="money" placeholder="100 монет">
                </div>
                <button type="submit" class="page-button">Пополнить счет</button>
            </form>

        </div>
    </div>
    <!-- END main-box -->