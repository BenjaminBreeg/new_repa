<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Связь с администрацией';
?>
<div class="main-box main-custom change admin-mail">
    <div class="container">
        <form action="" class="form text-left">
            <textarea type="text" class="form-control message" name="" rows="8" placeholder="Cообщение"></textarea>
            <a href="#" class="page-button save-btn save">Отправить</a>
        </form>
    </div>
</div>
<!-- END main-box -->