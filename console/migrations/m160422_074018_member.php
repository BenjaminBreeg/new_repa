<?php

use yii\db\Migration;

class m160422_074018_member extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%member}}', [
            'id' => $this->primaryKey(),
            'family' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'surname' =>$this->string(),
            'age' => $this->integer()->notNull(),
            'nationality' => $this->string()->notNull(),
            'passport' => $this->string()->notNull(),
            'passport_date' => $this->string()->notNull(),
            'bithday' => $this->string(),
            'birthplace' => $this->string(),
            'country' => $this->string(),
            'city' => $this->string(),
            'education' => $this->string(),
            'education_last' => $this->string(),
            'profession' => $this->string(),
            'workplace' => $this->string(),
            'position' => $this->string(),
            'family_status' => $this->string(),
            'child' => $this->string(),
            'talants' => $this->string(),
            'habbits' => $this->string(),
            'advantages' => $this->string(),
            'disadvantages' => $this->string(),
            'crossgender' => $this->string(),
            'why' => $this->string(),
            'video' => $this->string(),
            'foto' => $this->string(),
        ], $tableOptions);
    }

    public function down()
    {

        $this->dropTable('{{%member}}');
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
