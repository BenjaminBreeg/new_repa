<?php

use yii\db\Migration;

class m160425_074012_delete_user extends Migration
{
    public function up()
    {
        $this->dropTable('{{%user}}');
        return false;
    }

    public function down()
    {
        return false;
    }
}
