<?php

use yii\db\Migration;

class m160423_143121_create_news extends Migration
{
    public function up()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'text' => $this->text(),
            'created_at' => $this->dateTime()
        ]);
    }

    public function down()
    {
        $this->dropTable('news');
    }
}
